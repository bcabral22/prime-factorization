#run1
primeFact1=[2,2,3,3]
#run2
primeFact2=[2,2,5,5]

#getting the divisors
def getPostiveDivisors(X):
   #making list to show at the end
    og=[]
    #copying the list
    og=X.copy()
    #making a list of divisors that possibly repeat
    repeats=[]
    #offical list
    divsors=[]
    #will be used to get original number
    number= 1
    #for loop to get original number
    for i in X:
        number= number *  i
    #to keep the original number    
    reset=number 
    #wile loop to keep repting to go through prime list   
    while True:
        #reseting number to original
        number=reset
        #for loop to keep divind and adding to repeats list
        for div in range(0,len(X)):
            c= int(number/X[div])
            repeats.append(c)
            number=c
        #delete number in the 0 index    
        del X[0]   
        #once the size of list is zero aka empty then break while loop
        if len(X)==0:
            break
     #for loop to check the repeats   
    for delete in repeats:    
        #if number is not in divsors list then add it 
        if delete not in divsors:
            divsors.append(delete)
    #add the original number to the list
    divsors.append(reset)
    return print("Prime factorization:\n",og,"\nNumber:\n",reset,"\nSorted Divisors:\n", sorted(divsors),"\n")

getPostiveDivisors(primeFact1)      
getPostiveDivisors(primeFact2)